package com.example.frontczt.cleanarchitecture.inject

import com.example.frontczt.cleanarchitecture.injection.GatewayComponent
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [TestGatewayModule::class])
interface TestGatewayComponents : GatewayComponent