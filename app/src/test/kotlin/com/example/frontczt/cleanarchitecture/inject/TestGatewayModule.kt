package com.example.frontczt.cleanarchitecture.inject

import com.example.frontczt.cleanarchitecture.gateway.DatabaseGateway
import com.example.frontczt.cleanarchitecture.gateway.ServerGateway
import dagger.Module
import dagger.Provides
import org.mockito.Mockito
import javax.inject.Singleton

@Module
class TestGatewayModule {

    @Provides
    @Singleton
    fun provideDatabase(): DatabaseGateway {
        return Mockito.mock(DatabaseGateway::class.java)
    }

    @Provides
    @Singleton
    fun provideServer(): ServerGateway {
        return Mockito.mock(ServerGateway::class.java)
    }
}