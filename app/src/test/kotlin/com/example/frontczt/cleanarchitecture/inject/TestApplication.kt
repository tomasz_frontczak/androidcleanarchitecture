package com.example.frontczt.cleanarchitecture.inject

import com.example.frontczt.cleanarchitecture.MyApplication

class TestApplication : MyApplication() {

    override fun onCreate() {
        super.onCreate()
        gatewayComponents = DaggerTestGatewayComponents.builder()
            .testGatewayModule(TestGatewayModule())
            .build()
    }
}