package com.example.frontczt.cleanarchitecture

import android.app.Application
import androidx.test.core.app.ApplicationProvider
import com.example.frontczt.cleanarchitecture.database.data.Holder
import com.example.frontczt.cleanarchitecture.database.data.ItemType
import com.example.frontczt.cleanarchitecture.database.data.ItemTypeHolder
import com.example.frontczt.cleanarchitecture.inject.TestApplication
import com.example.frontczt.cleanarchitecture.pojo.TableHolder
import com.example.frontczt.cleanarchitecture.pojo.TableItemType
import com.example.frontczt.cleanarchitecture.pojo.TableItemTypeHolder
import com.example.frontczt.cleanarchitecture.pojo.Tables
import com.example.frontczt.cleanarchitecture.usecase.FetchDataUseCase
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.schedulers.TestScheduler
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import java.util.concurrent.TimeUnit

@RunWith(RobolectricTestRunner::class)
@Config(application = TestApplication::class)
class FetchUseCaseTest {

    lateinit var fetchUseCase: FetchDataUseCase
    lateinit var rxScheduler: TestScheduler

    @Before
    fun before() {
        rxScheduler = TestScheduler()
        fetchUseCase = FetchDataUseCase(ApplicationProvider.getApplicationContext<Application>(), rxScheduler)
    }

    @Test
    fun firstFetchData() {
        // given
        Mockito.`when`(fetchUseCase.databaseGateway.isEmpty()).thenReturn(Flowable.just(true))

        val table = mock(Tables::class.java)

        val holder = mock(Holder::class.java)
        val tableHolder = mock(TableHolder::class.java)
        Mockito.`when`(table.tableHolders).thenReturn(tableHolder)
        Mockito.`when`(tableHolder.holders).thenReturn(listOf(holder))

        val itemType = mock(ItemType::class.java)
        val tableItemType = mock(TableItemType::class.java)
        Mockito.`when`(table.tableItemTypes).thenReturn(tableItemType)
        Mockito.`when`(tableItemType.itemTypes).thenReturn(listOf(itemType))

        val itemTypeHolder = mock(ItemTypeHolder::class.java)
        val tableItemTypeHolder = mock(TableItemTypeHolder::class.java)
        Mockito.`when`(table.tableItemTypeHolder).thenReturn(tableItemTypeHolder)
        Mockito.`when`(tableItemTypeHolder.itemTypeHolders).thenReturn(listOf(itemTypeHolder))

        Mockito.`when`(fetchUseCase.serverGateway.getData()).thenReturn(Observable.just(table))

        Mockito.`when`(fetchUseCase.databaseGateway.getHoldersWithQuantity()).thenReturn(Flowable.empty())
        Mockito.`when`(fetchUseCase.databaseGateway.getItemsWithQuantity()).thenReturn(Flowable.empty())

        // when
        fetchUseCase.fetchData()
        rxScheduler.advanceTimeBy(1, TimeUnit.MILLISECONDS)

        // then
        verify(fetchUseCase.databaseGateway).insert(holder)
        verify(fetchUseCase.databaseGateway).insert(itemType)
        verify(fetchUseCase.databaseGateway).insert(itemTypeHolder)
    }
}