package com.example.frontczt.cleanarchitecture.view

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.frontczt.cleanarchitecture.R
import com.example.frontczt.cleanarchitecture.database.data.HolderWithQuantity
import com.example.frontczt.cleanarchitecture.interactor.Interactor
import com.example.frontczt.cleanarchitecture.viewmodel.RecordsViewModel
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_records.*

class LocationsFragment : Fragment() {

    private val noItemsAvailableMsg = "No items found"
    private lateinit var viewModel: RecordsViewModel
    private lateinit var viewAdapter: HolderRecordsAdapter
    private lateinit var interactor: Interactor
    private val compositeDisposable = CompositeDisposable()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return LayoutInflater.from(activity).inflate(R.layout.fragment_records, null)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        interactor = Interactor(fragmentManager!!, R.id.container)
        viewModel = ViewModelProviders.of(activity!!).get(RecordsViewModel::class.java)
        viewAdapter = HolderRecordsAdapter(viewModel)
        val disposable = viewAdapter.onClickProcessor.subscribe {
            if (it.itemTypeHolder.sumBy { it.Quantity } > 0) {
                interactor.openHolderDetails(it.holder.HolderId)
            } else {
                Snackbar.make(activity!!.findViewById(R.id.coordinatorLayout), noItemsAvailableMsg, Snackbar.LENGTH_SHORT).show()
            }
        }
        compositeDisposable.add(disposable)
        recordsView.layoutManager = LinearLayoutManager(context)
        recordsView.adapter = viewAdapter
        recordsView.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))

        val holdersDataObserver = Observer<List<HolderWithQuantity>> { newData ->
            if (newData != null) {
                viewAdapter.data = newData
                viewAdapter.notifyDataSetChanged()
            }
        }
        viewModel.holdersWithQuantity.observe(this, holdersDataObserver)
        viewModel.fetchHoldersWithQuantity()
        viewModel.fetchData()
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.clear()
    }

    class HolderRecordsAdapter(private val viewModel: RecordsViewModel) : RecordsAdapter<HolderWithQuantity>(viewModel) {

        override fun onBindViewHolder(viewHolder: RecordsViewHolder, position: Int) {
            viewHolder.name.text = data[position].holder.Name
            viewHolder.code.text = data[position].holder.Barcode
            val quantity = quantity(data[position].itemTypeHolder)
            viewHolder.quantity.text = "${quantity.first} (${quantity.second})"
            viewHolder.rowLayout.setOnClickListener { onClickProcessor.onNext(data[position]) }
        }
    }
}