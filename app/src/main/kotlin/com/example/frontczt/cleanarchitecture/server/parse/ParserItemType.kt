package com.example.frontczt.cleanarchitecture.server.parse

import com.example.frontczt.cleanarchitecture.database.data.ItemType
import com.example.frontczt.cleanarchitecture.pojo.Table
import com.example.frontczt.cleanarchitecture.pojo.TableItemType
import com.google.gson.JsonArray
import com.google.gson.JsonElement

class ParserItemType : Parser<ItemType>("ItemType") {

    override fun handle(json: JsonElement): Table {
        return TableItemType(parse(json))
    }

    override fun convertToPojoTable(json: JsonArray): ItemType {
        var itemType = ItemType()
        itemType.ItemTypeId = json[0].asInt
        itemType.ItemCode = json[1].asString
        itemType.Name = json[2].asString
        itemType.Description = json[3].asString
        itemType.Barcode = json[4].asString
        // ...
        return itemType
    }
}