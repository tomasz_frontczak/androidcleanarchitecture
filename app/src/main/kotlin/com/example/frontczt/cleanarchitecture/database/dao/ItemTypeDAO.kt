package com.example.frontczt.cleanarchitecture.database.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import android.arch.persistence.room.Transaction
import com.example.frontczt.cleanarchitecture.database.data.ItemType
import com.example.frontczt.cleanarchitecture.database.data.ItemTypeWithQuantity

@Dao
interface ItemTypeDAO {

    @Insert
    fun insert(itemType: ItemType)

    @Transaction
    @Query("SELECT * FROM ItemType")
    fun itemsWithQuantity(): List<ItemTypeWithQuantity>

    @Transaction
    @Query("SELECT * FROM ItemType, ItemTypeHolder WHERE HolderId LIKE :holderId ")
    fun itemsWithQuantityInLocation(holderId: Int): List<ItemTypeWithQuantity>
}