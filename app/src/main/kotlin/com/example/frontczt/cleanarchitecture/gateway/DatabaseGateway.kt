package com.example.frontczt.cleanarchitecture.gateway

import com.example.frontczt.cleanarchitecture.database.data.Holder
import com.example.frontczt.cleanarchitecture.database.data.HolderWithQuantity
import com.example.frontczt.cleanarchitecture.database.data.ItemType
import com.example.frontczt.cleanarchitecture.database.data.ItemTypeHolder
import com.example.frontczt.cleanarchitecture.database.data.ItemTypeWithQuantity
import io.reactivex.Flowable

interface DatabaseGateway {

    fun insert(holder: Holder)
    fun insert(itemType: ItemType)
    fun insert(itemTypeHolder: ItemTypeHolder)

    fun isEmpty(): Flowable<Boolean>

    fun getHoldersWithQuantity(): Flowable<List<HolderWithQuantity>>
    fun getItemsWithQuantity(): Flowable<List<ItemTypeWithQuantity>>
    fun itemsWithQuantityInLocation(holderId: Int): Flowable<List<ItemTypeWithQuantity>>
}