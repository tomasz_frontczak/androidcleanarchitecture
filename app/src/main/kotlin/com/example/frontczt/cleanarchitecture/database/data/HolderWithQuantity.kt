package com.example.frontczt.cleanarchitecture.database.data

import android.arch.persistence.room.Embedded
import android.arch.persistence.room.Relation

class HolderWithQuantity {

    @Embedded
    lateinit var holder: Holder

    @Relation(parentColumn = "HolderId", entityColumn = "HolderId", entity = ItemTypeHolder::class)
    lateinit var itemTypeHolder: List<ItemTypeHolder>
}