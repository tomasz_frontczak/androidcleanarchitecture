package com.example.frontczt.cleanarchitecture.interactor

import android.support.v4.app.FragmentManager
import com.example.frontczt.cleanarchitecture.view.ItemsInLocationFragment

class Interactor(val fragmentManager: FragmentManager, val containerID: Int) {

    fun openHolderDetails(holderId: Int) {
        val holderDetailsFragment = ItemsInLocationFragment()
        holderDetailsFragment.holderId = holderId
        fragmentManager.beginTransaction().add(containerID, holderDetailsFragment).addToBackStack("TAG").commit()
    }
}