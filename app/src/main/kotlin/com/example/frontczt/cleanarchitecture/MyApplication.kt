package com.example.frontczt.cleanarchitecture

import android.app.Application
import com.example.frontczt.cleanarchitecture.injection.DaggerGatewayComponent
import com.example.frontczt.cleanarchitecture.injection.GatewayComponent
import com.example.frontczt.cleanarchitecture.injection.GatewayModule

open class MyApplication : Application() {

    lateinit var gatewayComponents: GatewayComponent

    override fun onCreate() {
        super.onCreate()
        gatewayComponents = DaggerGatewayComponent.builder()
            .gatewayModule(GatewayModule(this))
            .build()
    }
}