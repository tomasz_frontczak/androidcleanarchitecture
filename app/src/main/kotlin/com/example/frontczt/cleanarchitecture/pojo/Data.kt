package com.example.frontczt.cleanarchitecture.pojo

import com.google.gson.annotations.SerializedName

class Data {

    @SerializedName("t")
    var tables: List<Table> = listOf()
}