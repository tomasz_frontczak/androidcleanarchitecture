package com.example.frontczt.cleanarchitecture.server.parse

import com.example.frontczt.cleanarchitecture.database.data.ItemTypeHolder
import com.example.frontczt.cleanarchitecture.pojo.Table
import com.example.frontczt.cleanarchitecture.pojo.TableItemTypeHolder
import com.google.gson.JsonArray
import com.google.gson.JsonElement

class ParserItemTypeHolder : Parser<ItemTypeHolder>("ItemTypeHolder") {

    override fun handle(json: JsonElement): Table {
        return TableItemTypeHolder(parse(json))
    }

    override fun convertToPojoTable(json: JsonArray): ItemTypeHolder {
        var itemTypeHolder = ItemTypeHolder()
        itemTypeHolder.ItemTypeHolderId = json[0].asInt
        itemTypeHolder.ItemTypeId = json[1].asInt
        itemTypeHolder.HolderId = json[2].asInt
        itemTypeHolder.Quantity = json[3].asInt
        itemTypeHolder.PickQuantity = json[4].asInt
        return itemTypeHolder
    }
}