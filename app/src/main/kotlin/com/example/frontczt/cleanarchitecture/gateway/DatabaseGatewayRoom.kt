package com.example.frontczt.cleanarchitecture.gateway

import android.app.Application
import android.arch.persistence.room.Room
import com.example.frontczt.cleanarchitecture.database.AppDatabase
import com.example.frontczt.cleanarchitecture.database.data.Holder
import com.example.frontczt.cleanarchitecture.database.data.HolderWithQuantity
import com.example.frontczt.cleanarchitecture.database.data.ItemType
import com.example.frontczt.cleanarchitecture.database.data.ItemTypeHolder
import com.example.frontczt.cleanarchitecture.database.data.ItemTypeWithQuantity
import io.reactivex.Flowable

open class DatabaseGatewayRoom(val application: Application) : DatabaseGateway {

    private val databaseName = "nameOfDb"
    val database: AppDatabase

    init {
        database = Room.databaseBuilder(application, AppDatabase::class.java, databaseName).build()
    }

    override fun insert(holder: Holder) {
        database.holderDao().insert(holder)
    }

    override fun insert(itemType: ItemType) {
        database.itemTypeDao().insert(itemType)
    }

    override fun insert(itemTypeHolder: ItemTypeHolder) {
        database.itemTypeHolder().insert(itemTypeHolder)
    }

    override fun isEmpty(): Flowable<Boolean> {
        return Flowable.fromCallable { (database.itemTypeHolder().countAll() == 0) }
    }

    override fun getHoldersWithQuantity(): Flowable<List<HolderWithQuantity>> {
        return Flowable.fromCallable { database.holderDao().holdersWithQuantity() }
    }

    override fun getItemsWithQuantity(): Flowable<List<ItemTypeWithQuantity>> {
        return Flowable.fromCallable { database.itemTypeDao().itemsWithQuantity() }
    }

    override fun itemsWithQuantityInLocation(holderId: Int): Flowable<List<ItemTypeWithQuantity>> {
        return Flowable.fromCallable { database.itemTypeDao().itemsWithQuantityInLocation(holderId) }
    }
}