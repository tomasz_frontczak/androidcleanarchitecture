package com.example.frontczt.cleanarchitecture.view

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.frontczt.cleanarchitecture.R
import com.example.frontczt.cleanarchitecture.database.data.ItemTypeWithQuantity
import com.example.frontczt.cleanarchitecture.viewmodel.RecordsViewModel
import kotlinx.android.synthetic.main.fragment_records.*

open class ItemsBaseFragment : Fragment() {

    lateinit var viewModel: RecordsViewModel
    lateinit var viewAdapter: ItemsRecordsAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return LayoutInflater.from(activity).inflate(R.layout.fragment_records, null)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProviders.of(activity!!).get(RecordsViewModel::class.java)
        viewAdapter = ItemsRecordsAdapter(viewModel)
        recordsView.layoutManager = LinearLayoutManager(context)
        recordsView.adapter = viewAdapter
        recordsView.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))

        val itemsDataObserver = Observer<List<ItemTypeWithQuantity>> { newData ->
            if (newData != null) {
                viewAdapter.data = newData
                viewAdapter.notifyDataSetChanged()
            }
        }
        viewModel.itemsWithQuantity.observe(this, itemsDataObserver)
    }

    class ItemsRecordsAdapter(private val viewModel: RecordsViewModel) : RecordsAdapter<ItemTypeWithQuantity>(viewModel) {

        override fun onBindViewHolder(viewHolder: RecordsViewHolder, position: Int) {
            viewHolder.name.text = data[position].itemType.Name
            viewHolder.code.text = data[position].itemType.Barcode
            val quantity = quantity(data[position].itemTypeHolder)
            viewHolder.quantity.text = "${quantity.first} (${quantity.second})"
        }
    }
}