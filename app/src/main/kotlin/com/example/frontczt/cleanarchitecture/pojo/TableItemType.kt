package com.example.frontczt.cleanarchitecture.pojo

import com.example.frontczt.cleanarchitecture.database.data.ItemType

open class TableItemType(open val itemTypes: List<ItemType>) : Table()