package com.example.frontczt.cleanarchitecture.view

import android.os.Bundle
import android.view.View

class ItemsFragment : ItemsBaseFragment() {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.fetchItemsWithQuantity()
    }
}