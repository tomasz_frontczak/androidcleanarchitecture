package com.example.frontczt.cleanarchitecture.database.data

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity
open class ItemType {

    @PrimaryKey var ItemTypeId: Int = -1
    var ItemCode: String = ""
    var Name: String = ""
    var Description: String = ""
    var Barcode: String = ""
    var DefaultSuppliersPartNumber: String = ""
    var HasSerialNumbers: Int = 0
    var UseManufacturersSerialNumber: Int = 0
    var Weight: Double = 0.0
    var WeightUnitId: Int = 0
    var Height: Double = 0.0
    var Width: Double = 0.0
    var Depth: Double = 0.0
    var attribute1: String = ""
    var attribute2: String = ""
    var attribute3: String = ""
    var attribute4: String = ""
    var attribute5: String = ""
    var attribute6: String = ""
    var attribute7: String = ""
    var attribute8: String = ""
    var attribute9: String = ""
    var attribute10: String = ""
    var attribute11: String = ""
    var attribute12: String = ""
    var attribute13: String = ""
    var attribute14: String = ""
    var attribute15: String = ""
}