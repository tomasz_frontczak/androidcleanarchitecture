package com.example.frontczt.cleanarchitecture.pojo

open class Tables(open val tableHolders: TableHolder, open val tableItemTypes: TableItemType, open val tableItemTypeHolder: TableItemTypeHolder)