package com.example.frontczt.cleanarchitecture.database.data

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity
open class Holder {

    @PrimaryKey var HolderId: Int = -1
    var HolderTypeId: Int = -1
    var Barcode: String = ""
    var Name: String = ""
    var QuantityOfItems: Int = 0
    var QuantityOfPickedItems: Int = 0
    var SiteId: Int = -1
    var Sequence: String? = null
    var StorageTypeId: String? = null
    var IsLocked: Int = 0
    var LocationUseTypeId: Int = -1
    var ParentHolderId: Int? = null
    var ParentHolderName: String? = null
    var ZoneId: Int? = null
    var LocationGroupId: Int? = null
}