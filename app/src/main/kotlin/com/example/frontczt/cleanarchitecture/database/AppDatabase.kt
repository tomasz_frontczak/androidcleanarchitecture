package com.example.frontczt.cleanarchitecture.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.example.frontczt.cleanarchitecture.database.dao.HolderDAO
import com.example.frontczt.cleanarchitecture.database.dao.ItemTypeDAO
import com.example.frontczt.cleanarchitecture.database.dao.ItemTypeHolderDao
import com.example.frontczt.cleanarchitecture.database.data.Holder
import com.example.frontczt.cleanarchitecture.database.data.ItemType
import com.example.frontczt.cleanarchitecture.database.data.ItemTypeHolder

@Database(entities = [Holder::class, ItemType::class, ItemTypeHolder::class], version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract fun holderDao(): HolderDAO
    abstract fun itemTypeDao(): ItemTypeDAO
    abstract fun itemTypeHolder(): ItemTypeHolderDao
}