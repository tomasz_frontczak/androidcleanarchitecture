package com.example.frontczt.cleanarchitecture.pojo

import com.example.frontczt.cleanarchitecture.database.data.ItemTypeHolder

open class TableItemTypeHolder(open val itemTypeHolders: List<ItemTypeHolder>) : Table()