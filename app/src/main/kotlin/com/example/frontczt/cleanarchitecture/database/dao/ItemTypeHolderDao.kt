package com.example.frontczt.cleanarchitecture.database.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import com.example.frontczt.cleanarchitecture.database.data.ItemTypeHolder

@Dao
interface ItemTypeHolderDao {

    @Insert
    fun insert(itemTypeHolder: ItemTypeHolder)

    @Query("SELECT count(*) FROM ItemTypeHolder")
    fun countAll(): Int
}