package com.example.frontczt.cleanarchitecture.gateway

import com.example.frontczt.cleanarchitecture.pojo.Tables
import io.reactivex.Observable

interface ServerGateway {

    fun getData(): Observable<Tables>
}
