package com.example.frontczt.cleanarchitecture.server.parse

import com.example.frontczt.cleanarchitecture.pojo.Table
import com.google.gson.JsonArray
import com.google.gson.JsonElement

abstract class Parser<T>(val tableName: String) {

    fun accept(json: JsonElement): Boolean {
        return json.asJsonObject.get("n").asString == tableName
    }

    fun parse(json: JsonElement): List<T> {
        var results = mutableListOf<T>()
        val jsonTable = json.asJsonObject.get("r").asJsonArray
        for (jsonRow in jsonTable) {
            val data = jsonRow.asJsonObject.get("v").asJsonArray
            results.add(convertToPojoTable(data))
        }
        return results
    }

    abstract fun handle(json: JsonElement): Table

    abstract fun convertToPojoTable(json: JsonArray): T
}