package com.example.frontczt.cleanarchitecture.pojo

import com.example.frontczt.cleanarchitecture.database.data.Holder

open class TableHolder(open val holders: List<Holder>) : Table()