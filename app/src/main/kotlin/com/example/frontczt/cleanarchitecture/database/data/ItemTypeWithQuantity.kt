package com.example.frontczt.cleanarchitecture.database.data

import android.arch.persistence.room.Embedded
import android.arch.persistence.room.Relation

class ItemTypeWithQuantity {

    @Embedded
    lateinit var itemType: ItemType

    @Relation(parentColumn = "ItemTypeId", entityColumn = "ItemTypeId", entity = ItemTypeHolder::class)
    lateinit var itemTypeHolder: List<ItemTypeHolder>
}