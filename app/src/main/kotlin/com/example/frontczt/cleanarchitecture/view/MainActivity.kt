package com.example.frontczt.cleanarchitecture.view

import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.example.frontczt.cleanarchitecture.R
import kotlinx.android.synthetic.main.activity_main.*

private const val MENU_ITEMS = 2
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        pager.adapter = FragmentsAdapter(this, supportFragmentManager)
        tablayout.setupWithViewPager(pager)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    class FragmentsAdapter(val context: Context, fragmentManager: FragmentManager) : FragmentPagerAdapter(fragmentManager) {

        override fun getItem(position: Int): Fragment {
            if (position == 0) return LocationsFragment()
            else return ItemsFragment()
        }

        override fun getCount(): Int {
            return MENU_ITEMS
        }

        override fun getPageTitle(position: Int): CharSequence? {
            if (position == 0) return context.getString(R.string.menu_locations)
            else return context.getString(R.string.menu_items)
        }
    }
}
