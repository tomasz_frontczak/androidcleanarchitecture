package com.example.frontczt.cleanarchitecture.database.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import android.arch.persistence.room.Transaction
import com.example.frontczt.cleanarchitecture.database.data.Holder
import com.example.frontczt.cleanarchitecture.database.data.HolderWithQuantity

@Dao
interface HolderDAO {

    @Insert
    fun insert(holder: Holder)

    @Transaction
    @Query("SELECT * FROM Holder")
    fun holdersWithQuantity(): List<HolderWithQuantity>
}