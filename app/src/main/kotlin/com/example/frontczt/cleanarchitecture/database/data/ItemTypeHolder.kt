package com.example.frontczt.cleanarchitecture.database.data

import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.PrimaryKey

@Entity
open class ItemTypeHolder {

    @PrimaryKey var ItemTypeHolderId: Int = -1
    @ForeignKey(entity = ItemType::class, parentColumns = ["ItemTypeId"], childColumns = ["ItemTypeId"]) var ItemTypeId: Int = -1
    @ForeignKey(entity = Holder::class, parentColumns = ["HolderId"], childColumns = ["HolderId"]) var HolderId: Int = -1
    var Quantity: Int = 0
    var PickQuantity: Int = 0
}