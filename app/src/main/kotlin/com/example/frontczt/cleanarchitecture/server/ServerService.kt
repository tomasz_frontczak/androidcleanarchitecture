package com.example.frontczt.cleanarchitecture.server

import com.example.frontczt.cleanarchitecture.pojo.Data
import io.reactivex.Observable
import retrofit2.http.GET

interface ServerService {

    @GET("test.json")
    fun getData(): Observable<Data>
}