package com.example.frontczt.cleanarchitecture.viewmodel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import com.example.frontczt.cleanarchitecture.database.data.HolderWithQuantity
import com.example.frontczt.cleanarchitecture.database.data.ItemTypeWithQuantity
import com.example.frontczt.cleanarchitecture.usecase.FetchDataUseCase
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class RecordsViewModel(application: Application) : AndroidViewModel(application) {

    private val fetchDataUseCase = FetchDataUseCase(application)
    private val compositeDisposable = CompositeDisposable()

    val holdersWithQuantity: MutableLiveData<List<HolderWithQuantity>> by lazy {
        MutableLiveData<List<HolderWithQuantity>>()
    }

    val itemsWithQuantity: MutableLiveData<List<ItemTypeWithQuantity>> by lazy {
        MutableLiveData<List<ItemTypeWithQuantity>>()
    }

    fun fetchHoldersWithQuantity() {
        val disposable = fetchDataUseCase.getHoldersWithQuantity()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { holdersWithQuantity.value = it }
        compositeDisposable.add(disposable)
    }

    fun fetchItemsWithQuantity() {
        val disposable = fetchDataUseCase.getItemsWithQuantity()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { itemsWithQuantity.value = it }
        compositeDisposable.add(disposable)
    }

    fun fetchItemsInLocation(holderId: Int) {
        val disposable = fetchDataUseCase.getItemsWithQuantityInLocation(holderId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { itemsWithQuantity.value = it }
        compositeDisposable.add(disposable)
    }

    fun fetchData() {
        fetchDataUseCase.fetchData()
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
        fetchDataUseCase.onCleared()
    }
}