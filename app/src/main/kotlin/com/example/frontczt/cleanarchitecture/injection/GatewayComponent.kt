package com.example.frontczt.cleanarchitecture.injection

import com.example.frontczt.cleanarchitecture.usecase.FetchDataUseCase
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [GatewayModule::class])
interface GatewayComponent {

    fun inject(fetchUseCase: FetchDataUseCase)
}