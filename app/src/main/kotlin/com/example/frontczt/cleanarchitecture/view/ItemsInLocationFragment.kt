package com.example.frontczt.cleanarchitecture.view

import android.os.Bundle
import android.view.View

class ItemsInLocationFragment : ItemsBaseFragment() {

    var holderId: Int = -1

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.fetchItemsInLocation(0)
    }
}