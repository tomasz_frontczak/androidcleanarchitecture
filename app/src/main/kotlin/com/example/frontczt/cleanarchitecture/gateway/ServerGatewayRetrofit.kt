package com.example.frontczt.cleanarchitecture.gateway

import com.example.frontczt.cleanarchitecture.pojo.Data
import com.example.frontczt.cleanarchitecture.pojo.Table
import com.example.frontczt.cleanarchitecture.pojo.Tables
import com.example.frontczt.cleanarchitecture.pojo.TableHolder
import com.example.frontczt.cleanarchitecture.pojo.TableItemType
import com.example.frontczt.cleanarchitecture.pojo.TableItemTypeHolder
import com.example.frontczt.cleanarchitecture.server.ServerDeserializer
import com.example.frontczt.cleanarchitecture.server.ServerService
import com.google.gson.GsonBuilder
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

open class ServerGatewayRetrofit : ServerGateway {

    private val serverUrl = ""
    private val service: ServerService

    init {
        val retrofit = Retrofit.Builder()
            .baseUrl(serverUrl)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(createConverter())
            .build()
        service = retrofit.create(ServerService::class.java)
    }

    private fun createConverter(): Converter.Factory {
        val gsonBuilder = GsonBuilder()
        gsonBuilder.registerTypeAdapter(Table::class.java, ServerDeserializer())
        val gson = gsonBuilder.create()
        return GsonConverterFactory.create(gson)
    }

    override fun getData(): Observable<Tables> {
        return service.getData()
            .retry(3)
            .subscribeOn(Schedulers.io())
            .map { convertToTables(it) }
    }

    private fun convertToTables(data: Data): Tables {
        lateinit var tableHolder: TableHolder
        lateinit var tableItemType: TableItemType
        lateinit var tableItemTypeHolder: TableItemTypeHolder
        for (table in data.tables) {
            if (table is TableHolder) {
                tableHolder = table
            } else if (table is TableItemType) {
                tableItemType = table
            } else if (table is TableItemTypeHolder) {
                tableItemTypeHolder = table
            }
        }
        return Tables(tableHolder, tableItemType, tableItemTypeHolder)
    }
}
