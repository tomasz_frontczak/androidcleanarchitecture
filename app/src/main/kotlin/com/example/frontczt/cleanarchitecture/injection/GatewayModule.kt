package com.example.frontczt.cleanarchitecture.injection

import android.app.Application
import com.example.frontczt.cleanarchitecture.gateway.DatabaseGateway
import com.example.frontczt.cleanarchitecture.gateway.DatabaseGatewayRoom
import com.example.frontczt.cleanarchitecture.gateway.ServerGateway
import com.example.frontczt.cleanarchitecture.gateway.ServerGatewayRetrofit
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class GatewayModule(val application: Application) {

    @Provides @Singleton fun provideDatabase(): DatabaseGateway {
        return DatabaseGatewayRoom(application)
    }

    @Provides @Singleton fun provideServer(): ServerGateway {
        return ServerGatewayRetrofit()
    }
}