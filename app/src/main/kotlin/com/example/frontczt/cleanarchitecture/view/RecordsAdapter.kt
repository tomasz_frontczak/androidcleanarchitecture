package com.example.frontczt.cleanarchitecture.view

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import com.example.frontczt.cleanarchitecture.R
import com.example.frontczt.cleanarchitecture.database.data.HolderWithQuantity
import com.example.frontczt.cleanarchitecture.database.data.ItemTypeHolder
import com.example.frontczt.cleanarchitecture.viewmodel.RecordsViewModel
import io.reactivex.processors.PublishProcessor

abstract class RecordsAdapter<T>(private val viewModel: RecordsViewModel) :
    RecyclerView.Adapter<RecordsAdapter.RecordsViewHolder>() {

    val onClickProcessor = PublishProcessor.create<HolderWithQuantity>()
    var data: List<T> = listOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecordsViewHolder {
        val layout = LayoutInflater.from(parent.context).inflate(R.layout.record_item_view, parent, false)
        return RecordsViewHolder(layout)
    }

    override fun getItemCount(): Int {
        return data.count()
    }

    fun quantity(itemTypeHolder: List<ItemTypeHolder>): Pair<Int, Int> {
        return Pair(itemTypeHolder.sumBy { it.Quantity }, itemTypeHolder.sumBy { it.PickQuantity })
    }

    class RecordsViewHolder(val rowLayout: View) : RecyclerView.ViewHolder(rowLayout) {

        val name: TextView = rowLayout.findViewById(R.id.holderName)
        val code: TextView = rowLayout.findViewById(R.id.holderCode)
        val quantity: TextView = rowLayout.findViewById(R.id.holderQuantity)
        val rowRecord: RelativeLayout = rowLayout.findViewById(R.id.record_item)
    }
}