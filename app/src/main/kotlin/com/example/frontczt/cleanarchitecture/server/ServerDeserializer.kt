package com.example.frontczt.cleanarchitecture.server

import com.example.frontczt.cleanarchitecture.pojo.Table
import com.example.frontczt.cleanarchitecture.server.parse.ParserHolder
import com.example.frontczt.cleanarchitecture.server.parse.ParserItemType
import com.example.frontczt.cleanarchitecture.server.parse.ParserItemTypeHolder
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import java.lang.RuntimeException
import java.lang.reflect.Type

class ServerDeserializer : JsonDeserializer<Table> {

    val parsers = arrayOf(ParserHolder(), ParserItemType(), ParserItemTypeHolder())

    override fun deserialize(
        json: JsonElement?,
        typeOfT: Type?,
        context: JsonDeserializationContext?
    ): Table {
        if (json != null) {
            for (parser in parsers) {
                if (parser.accept(json)) {
                    return parser.handle(json)
                }
            }
        }
        throw RuntimeException("No json data to parse")
    }
}