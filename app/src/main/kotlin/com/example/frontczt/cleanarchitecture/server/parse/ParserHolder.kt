package com.example.frontczt.cleanarchitecture.server.parse

import com.example.frontczt.cleanarchitecture.database.data.Holder
import com.example.frontczt.cleanarchitecture.pojo.Table
import com.example.frontczt.cleanarchitecture.pojo.TableHolder
import com.google.gson.JsonArray
import com.google.gson.JsonElement

class ParserHolder : Parser<Holder>("Holder") {

    override fun handle(json: JsonElement): Table {
        return TableHolder(parse(json))
    }

    override fun convertToPojoTable(json: JsonArray): Holder {
        var holder = Holder()
        holder.HolderId = json[0].asInt
        holder.HolderTypeId = json[1].asInt
        holder.Barcode = json[2].asString
        holder.Name = json[3].asString
        holder.QuantityOfItems = json[4].asInt
        // ...
        return holder
    }
}