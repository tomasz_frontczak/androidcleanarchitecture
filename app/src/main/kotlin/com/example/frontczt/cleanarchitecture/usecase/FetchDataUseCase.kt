package com.example.frontczt.cleanarchitecture.usecase

import android.app.Application
import android.util.Log
import com.example.frontczt.cleanarchitecture.MyApplication
import com.example.frontczt.cleanarchitecture.database.data.HolderWithQuantity
import com.example.frontczt.cleanarchitecture.database.data.ItemTypeWithQuantity
import com.example.frontczt.cleanarchitecture.gateway.DatabaseGateway
import com.example.frontczt.cleanarchitecture.gateway.ServerGateway
import com.example.frontczt.cleanarchitecture.pojo.Tables
import io.reactivex.Flowable
import io.reactivex.Scheduler
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.processors.BehaviorProcessor
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class FetchDataUseCase(application: Application, private val scheduler: Scheduler = Schedulers.io()) {

    private val itemTypeWithQuantitySubject = BehaviorProcessor.create<List<ItemTypeWithQuantity>>()
    private val holderWithQuantitySubject = BehaviorProcessor.create<List<HolderWithQuantity>>()
    private val compositeDisposable = CompositeDisposable()

    @Inject lateinit var serverGateway: ServerGateway
    @Inject lateinit var databaseGateway: DatabaseGateway

    init {
        (application as MyApplication).gatewayComponents.inject(this)
    }

    fun fetchData() {
        val disposable = databaseGateway
            .isEmpty()
            .subscribeOn(scheduler)
            .observeOn(scheduler)
            .subscribe { empty ->
                if (empty) {
                    try {
                        val tables = serverGateway.getData().blockingFirst()
                        saveTablesToDatabase(tables)
                    } catch (e: Exception) {
                        Log.e("ERROR", "Cant reach host", e)
                        return@subscribe
                    }
                }
                databaseGateway.getHoldersWithQuantity().subscribe(holderWithQuantitySubject)
                databaseGateway.getItemsWithQuantity().subscribe(itemTypeWithQuantitySubject)
            }
        compositeDisposable.add(disposable)
    }

    fun getItemsWithQuantity(): Flowable<List<ItemTypeWithQuantity>> {
        return itemTypeWithQuantitySubject
    }

    fun getHoldersWithQuantity(): Flowable<List<HolderWithQuantity>> {
        return holderWithQuantitySubject
    }

    fun getItemsWithQuantityInLocation(holderId: Int): Flowable<List<ItemTypeWithQuantity>> {
        return databaseGateway.itemsWithQuantityInLocation(holderId).subscribeOn(Schedulers.io())
    }

    fun onCleared() {
        compositeDisposable.clear()
    }

    private fun saveTablesToDatabase(tables: Tables) {
        for (holder in tables.tableHolders.holders) {
            databaseGateway.insert(holder)
        }
        for (itemType in tables.tableItemTypes.itemTypes) {
            databaseGateway.insert(itemType)
        }
        for (itemTypeHolder in tables.tableItemTypeHolder.itemTypeHolders) {
            databaseGateway.insert(itemTypeHolder)
        }
    }
}